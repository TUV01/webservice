package jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
@Path("/medicament")
public class MedicamentRating {
	@GET 
	@Path("/medrating")  
	@Produces(MediaType.APPLICATION_JSON) 
	public String medrating(@QueryParam("krankheit") String krankheit){
		Object[] obj = null;
		String response = "";
		if (getMedicament(krankheit)){
		try {
			obj = DBConnectionJDBC.getMedicamentRat(krankheit);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Integer[] Bewertung = (Integer[]) obj[0];
		String[] Medikament = (String[]) obj[1];

			response = Service.constructJSON(Bewertung, Medikament);
		} else {
			response = Service.constructJSON(false, "incorrect value");
		}
		// Return a JSON Object
		return response;

	}

	// creating empty Object to fill it with data from DB
	private boolean getMedicament(String krankheit){
		System.out.println("Inside checkCredentials");
		boolean isSuccess = false;
		if(Service.isNotNull(krankheit)){
			try {
					
					return true;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
		} else {
			isSuccess = false;
		
		}
		return isSuccess;
				

		
		
	}

}
