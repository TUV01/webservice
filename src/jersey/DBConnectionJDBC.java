package jersey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnectionJDBC {
	// Create Connection to DB
	@SuppressWarnings("finally")
	public static Connection createConnection() throws Exception {
		Connection con = null;
		try {
			Class.forName(DBConnectValues.dbClass);
			con = DriverManager.getConnection(DBConnectValues.dbUrl, DBConnectValues.dbUser, DBConnectValues.dbPwd);
		} catch (Exception e) {
			throw e;
		} finally {
			return con;
		}
	}
    
	// Method to check username and password
	public static boolean UserLogin(String username, String password) throws Exception {
		boolean isUserAvailable = false;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "select * from patienten where username = '" + username
					+ "' and password = '" + password + "'";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				isUserAvailable = true;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return isUserAvailable;
	}


	// Insert the User for registration
	public static boolean insertUser(String username, String password, String email, String firstName, String lastName, String status) throws SQLException, Exception {
		boolean registerStatus = false;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "insert into patienten (username, password, email, firstName, lastName, status) values("
                    + "'" + username + "'" + "," + "'" + password + "'" + "," + "'" + email + "'" + "," + "'" + firstName + "'" + "," + "'" + lastName + "'" + "," + "'" + status + "'" + ")";
			int records = stmt.executeUpdate(query);
			if (records > 0){
				registerStatus = true;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			
				dbConn.close();
			
		}
		return registerStatus;
	}
	
	// Login check for doctor account
	public static boolean DoctorLogin(String drUsername, String drPassword) throws Exception {
		boolean isDrAvailable = false;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "select * from doctor where drUsername = '" + drUsername + "' and drPassword = '" + drPassword + "'";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				isDrAvailable = true;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return isDrAvailable;
	}
	
	// Adding the Feedback from the user into DB
	public static boolean insertFeedback(String feedback, int rating) throws SQLException, Exception {
		boolean insertStatus = false;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "insert into Feedback (Feedback, Bewertung) values("
                    + "'" + feedback + "'" + "'" + rating + "'" + ")";
			int records = stmt.executeUpdate(query);
			if (records > 0) {
				insertStatus = true;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return insertStatus;
	}
	
	// Return two Array�s of MedicamentRating�s from the db
	public static Object[] getMedicamentRat(String krankheit) throws Exception {
		Integer[] bewertung = new Integer[5];
        String[] medikament = new String[5];
        int i = 1;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "select Medikament, Bewertung from Medikamente where Krankheit = '" + krankheit + "'";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				medikament[i-1] = rs.getString("Medikament");
                bewertung[i-1] = rs.getInt("Bewertung");
				i++;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return new Object[]{bewertung, medikament};
	}
	
	// return the status of the patient�s
	public static Object[] patientenStats() throws Exception {
		String[] lastName = new String[99];
		String[] firstName = new String[99];
		String[] status = new String[99];
		int i = 0;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "select lastName, firstName, status from patienten";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				lastName[i] = rs.getString("lastName");
                firstName[i] = rs.getString("firstName");
                status[i] = rs.getString("status");
				i++;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return new Object[]{lastName, firstName, status};
	}
	
	public static Object[] PatientHistory(String lastName, String firstName) throws Exception {
		String[] Date = new String[99];
		String[] Disease = new String[99];
		String[] Medicament = new String[99];
		int i = 0;
		Connection dbConn = null;
		try {
			try {
				dbConn = DBConnectionJDBC.createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Statement stmt = dbConn.createStatement();
			String query = "select * from history where Lastname = '" + lastName
					+ "' and Firstname = '" + firstName + "'";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
                Date[i] = rs.getString("Date");
                Disease[i] = rs.getString("Disease");
                Medicament[i] = rs.getString("Medicament");
				i++;
			}
		} catch (SQLException sqle) {
			throw sqle;
		} catch (Exception e) {
			if (dbConn != null) {
				dbConn.close();
			}
			throw e;
		} finally {
			if (dbConn != null) {
				dbConn.close();
			}
		}
		return new Object[]{Date, Disease, Medicament};
	}
	
	
}
